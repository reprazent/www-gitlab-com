require 'spec_helper'

describe Changelog::MergeRequest do
  let(:date) { DateTime.new(1970, 1, 1) }

  describe '#changes_handbook?' do
    subject { described_class.new(2, 'test') }

    before do
      subject.instance_variable_set(:@changes, {
                                      "id" => 1,
                                      "title" => "test",
                                      "changes" =>  [
                                        { "new_path" => "source/handbook/marketing/website/index.html.md" }
                                      ]
                                    })
    end

    it 'changed the handbook' do
      expect(subject.changes_handbook?).to be(true)
    end
  end

  describe '#to_s' do
    subject { described_class.new(2, 'test').to_s }

    it 'creates a markdown line' do
      expect(subject).to eq('- [!2](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2) test')
    end
  end
end
