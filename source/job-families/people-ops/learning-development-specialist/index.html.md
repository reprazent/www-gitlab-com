---
layout: job_page
title: "Learning & Development Partner"
---

The Learning & Development Partner will identify and design the right learning solutions to support GitLab's growth. They will have deep experience in developing programs, learning offerings and approaches, content creation, and consultation with partners and leaders to drive development and growth within the company. They will also build and scale a program that focuses on individual development, strengthening our leadership, and creating a culture of learning and development.

## Responsibilities

- Partner with People Business Partners and the leadership team to identify, develop, and deliver programs that support the growth of our team members and GitLab
- Research, develop, and facilitate exciting and impactful asynchronous, virtual, and experiential learning offerings
- Establish an internal learning and development roadmap for all GitLab managers and individual contributors
- Collaborate with the Sales team to design and deliver orientation content that develops new hires' understanding of GitLab’s business and platform
- Coordinate an effective marketing strategy to promote internal training
- Iterate on existing materials and develop new L&D content
- Select an exceptional e-learning platform for GitLab based on the needs our business and team members
- Create the course catalog which will include e-learning, instructor-led courses, and hands-on workshops, utilizing the GitLab handbook to ensure the trainings are accessible to everyone
- Create and design the supporting course material for all development programs, both for instructor-led and e-learning
- Solicit and incorporate team member feedback into our course content and experience

## Requirements

- 5+ years experience in instructional design and developing leadership learning content
- Track record of designing engaging and impactful development programs that improves individual, team, and company performance
- Exceptional written and interpersonal skills
- Experience developing self-service training content, such as e-learning modules
- Strong logistical planning and organizational skills
- Team-orientated, engaging, and energetic
- Capable of working collaboratively across multiple departments
- Passionate about personal development, training, learning, and seeing individuals develop to their fullest potential
- The ability to thrive in a fast-paced environment
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a 30-45 minute interview with member(s) of our People Operations team
- Next, candidates will be invited to schedule a 30 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 45 minute interview with our Chief Culture Officer
- Next, the candidate will be invited to interview with a People Business Partner.
- Finally, our CEO may choose to conduct a final interview
