---
layout: job_page
title: "Support Engineer"
---

## Support Engineering Roles at GitLab

Support Engineers work with Customers who use GitLab on-prem or in the cloud. They focus on getting bugs and feature proposals triaged and routed to the appropriate team. Support Engineers are often on the vanguard of new and interesting ways GitLab is used, deployed, and managed. Support Engineers sit at the intersection of Systems Administration, DevOps, and Customer Service.

### Junior Support Engineer

Junior Support Engineers share the same responsibilities outlined below, but typically
join with less or alternate experience in one of the key areas of Support Engineering
expertise (Systems Administration skills, Ruby on Rails, Git, and customer support). For example,
a person with some experience in a web framework other than RoR, but with experience
on the other three areas would typically join as a Junior.

### Intermediate Support Engineer

- Engage with our customers to triage customer issues via email and video conferencing
- Collaborate with our Product and Development Teams to build new features and get bugs fixed
- Create or update documentation based on customer interactions
- Participate in the on-call rotation to provide 24/7 emergency customer response
- Maintain good ticket performance and satisfaction
- Keen eye for forward thinking solutions (Kubernetes, Containers, etc.)
- Clear technical knowledge and comfort explaining technical concepts to various audiences
- Excellent spoken and written English
- Spoken and written English and Japanese, Korean or Mandarin for the APAC timezone
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).


### Senior Support Engineer

Senior Support Engineers are more experienced engineers who meet the following
criteria:

- Mentor Team Members on new technologies and new GitLab features
- Expert Debugging Skills
- Submit merge requests to resolve GitLab bugs
- Help hire and train new Support Engineers
- Drive feature requests based on customer interactions
- Process oriented: Suggest and implement improvements to the support workflow
- Contribute to one or more complementary projects

A Senior Support Engineer may be interested in exploring Support Management as an alternative at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

### Staff Support Engineer

A Senior Support Engineer will be promoted to Staff Support Engineer when they have
demonstrated significant leadership and impact; typically around resolving customer issues. This may
involve any type of consistent "above and beyond senior level" performance, for example:

-  Regularly submitting merge requests for customer reported/requested GitLab bugs and feature proposals
-  Working across functional groups to deliver on projects relating to customer experience and success.
-  Writing in-depth documentation and clarifying community communications that share knowledge and radiate GitLab's technical strengths
-  The ability to create innovative solutions that push GitLab's technical abilities ahead of the curve
-  Identifying significant projects that result in substantial cost savings or revenue
-  Proactively defining and solving important architectural issues based on extensive customer knowledge

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 90 minute technical interview with customer scenarios with a Support Engineer
* Candidates will then be invited to schedule a 45 minute behavioral interview with a Support Engineering Manager
* Candidates will then be invited to schedule a 45 minute behavioral interview with our Director of Support
* Candidates will then be invited to schedule an additional 45 minute interview with the VP of Engineering
* Finally, candidates may be asked to interview with the CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
