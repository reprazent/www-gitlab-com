---
layout: markdown_page
title: "Uploading Executed Contracts to ContractWorks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Steps to Filing Your New Vendor Contract in ContractWorks

This process is used to file your contract or related vendor documents after they are fully executed (signed by all parties).  For the full contract approval process, see [Procure to Pay](https://about.gitlab.com/handbook/finance/procure-to-pay/).

### Requesting a ContractWorks Log-in

If you have not used ContractWorks yet, assign your contract approval issue back to gl-legal-team and request a ContractWorks sign in. You will receive an e-mail invitation to log in and file your fully executed contract. 

  

### Uploading the Document

#### Step 1: Open the Correct File for Your Upload 

Use the folder navigation panel on the left side of your screen to open your department’s file. Some departments have subfolders for categories or vendors. If your contract would best fit in one of the subfolders, go ahead and open it up as well. 

#### Step 2: Upload Your Fully Executed Contract

Once you have the correct file selected, uploading is very simple. You can either drag and drop into the files panel on the right or use the blue `upload` button to browse your files. Please ensure that the contract version you upload is the signed pdf.

#### Step 3: Standardize Your File Name

Click on the pencil icon at the right-most edge of your file information line to edit the file name and name according to the example below:

  

Vendor Name-GitLab Contract Type - yyyy.mm.dd.pdf

  

There should not be any underscores in your file name. Spaces between words are fine. Contract Type will normally be the heading you find on the contract itself. Some examples include “Master Service Agreement”, “Partnership Incentive Agreement”, “Addendum to Subscription Terms”. The date should be the Effective Date, which is usually the date when the last signature is added (unless it is specified otherwise in the contract).

  

### Tagging the Document

#### Step 1: Choose the Correct Template for Your Contract Type.

Click on the greyed out tags in your file line. This will take you to a screen entitled “Manage Tags”. From the drop-down menu, select the template that best fits your contract. 

  

#### Step 2: Tag Your Contract.

Fill in the tags with the information you collected during the contract approval process. See details below for best practices for recurrence and notifications.

  

When you’ve filled everything in, click Submit.

  

### Attaching the Document

If your document is an Order Form or other sub-document that is connected to a Master Agreement, you will need to attach the document to the Master Agreement, as detailed below:

  

1. Go to the folder where your Order Form and Master Agreement are filed. (Ensure that your Order Form and Master agreement are filed in the same folder, as attaching doesn’t seem to work across different folders.) 
2. Select the new document by clicking the checkbox on the left end of the contract information line. This will make the options along the top of the file list blue and clickable. 
3. Click `Attach` 
4. Hover over the Master Agreement and click the paperclip that has appeared next to the edit pencil. 

### Working with Auto-Renewals

When you enter the start and end dates of your contract, you will have the option to set notifications and recurrences. 

#### Recurrences

Recurrences will change the start and/or end dates for your contract as it auto-renews. Simply select the option and set the number of months of your contract term. ContractWorks will then auto-change the dates when the appropriate time comes.

#### Notifications

To set notifications for start and end dates, select the `Enable Notifications` option. This will bring up 3 different fields to fill in. Follow the best practices below for each type of notification.

##### Start Date

1. Set the days to 180 
2. Input your e-mail address  
3. Use the following message: This contract will auto-renew in 6 months. If this vendor is meeting the needs you had for it, you do not need to do anything at this time. If you are not happy with how this vendor is working out, please begin researching other options. 

##### End Date

1. Set the days 30 day before termination notice is due. (For example, if we need to give 30 days’ notice, set the notification to 60 days.) 
2. Input your e-mail address and [legal@gitlab.com](mailto:legal@gitlab.com) 
3. Use the following message: This contract will auto-renew in xx days. If you do not wish to continue with this vendor, you must give x days’ notice.

### Linking to the Document

There may be times when you need to send your executed contract to another GitLab team member, such as when invoices need to be paid.

1. Navigate to your uploaded contract.
2. Click on the contract file name, which will open a watermarked version of the contract in a new tab.
3. Use the new tab's unique url to link the document.
 
