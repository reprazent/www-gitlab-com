### 2018-10-09
- [!15248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15248) Prioritization section for Manage team
- [!15242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15242) Clarify how to add yourself to team page
- [!15241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15241) added oct 2018 newsletter
- [!15058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15058) Jarv/add canary design

### 2018-10-08
- [!15233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15233) add send limit to outreach info
- [!15227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15227) How to credit blog posts
- [!15222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15222) Clarify criteria
- [!15220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15220) Why business cards for all.
- [!15215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15215) Adding information about how to become an interviewer.
- [!15214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15214) Add LinkedIn
- [!15209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15209) Add License Management report
- [!15203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15203) Fix typo in index.html.md
- [!15191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15191) update company and fgu call instructions
- [!15188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15188) updated TAM index page
- [!15139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15139) Initial CI/CD Infra Blueprint
- [!14841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14841) Add page outlining disaster recovery concepts.

### 2018-10-07
- [!15195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15195) Fix multiple pages w/ spelling formetting and links

### 2018-10-05
- [!15187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15187) Add ToC
- [!15181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15181) Jj xdr coaching
- [!15170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15170) Update handbook - move XDR content to separate dedicated page.
- [!15168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15168) Added instructions on how to request access to YouTube
- [!15166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15166) Update support handbook main page
- [!15165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15165) Update move content from main AR handbook page to this page.
- [!15162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15162) 20181005 mpm section updates
- [!15157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15157) add bizible definitions
- [!15155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15155) Add link to Marketing Programs
- [!15146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15146) Add Bizible definitions and usage
- [!15145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15145) update fgu to change names and add engage
- [!15143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15143) Update fixing some typos, adding dates to reports, adjusting layout
- [!15124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15124) Updated handbook typo
- [!15116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15116) #15 Update wording
- [!15073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15073) Update issue escalations workflow
- [!15064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15064) [Quality] Add a link to the known QA failures
- [!15030](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15030) add blurb around maintaining vision epic/issues up to date.
- [!15015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15015) Added link to vendor contract approval process
- [!14804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14804) Geo Planning Board link update

### 2018-10-04
- [!15141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15141) Resolve "New AR handbook subpage for XDR training"
- [!15132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15132) Clarify inactive issues - blog hb
- [!15131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15131) Remove Forrester VSM info, because link to VSM page is here.
- [!15128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15128) Fix a typo: chang -> change
- [!15126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15126) Add on-call summary description to SRE handbook page
- [!15123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15123) Velocity typo and clarification
- [!15119](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15119) add best practices
- [!15111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15111) Add events calendar
- [!15103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15103) Modify scoring & record creation
- [!15091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15091) Updated status info

### 2018-10-03
- [!15100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15100) fix tool page template and make website edit instructions talk about using it
- [!15092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15092) Adding a "the" as a candidate example.
- [!15081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15081) Add professional services rqst details
- [!15080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15080) Add example of standalone specialist job families
- [!15074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15074) Added link to Manage playlist
- [!15072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15072) Add crosslinks between sections talking about ambition
- [!15067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15067) Updates to CHD
- [!15066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15066) Added "mandatory" comment on details for event to be added
- [!15065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15065) Resolve "add workflow to blog handbook"
- [!15063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15063) add link to email policy in rules of engagement
- [!15061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15061) Link directly to the template
- [!15053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15053) Fix ERB templating in ops-backend/configure/index.html.md.erb
- [!15050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15050) Add email signature to tools-and-tips page
- [!15049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15049) add MPM details to duties chart
- [!15039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15039) Updating  the 360 page to remove Lattice
- [!15021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15021) Update hack day instructions
- [!14976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14976) added detail on trial process
- [!14935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14935) Added security facet in test planning.

### 2018-10-02
- [!15036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15036) add link to issue board
- [!15023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15023) Updating page to include instructions for specialist families
- [!15017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15017) Add blackout notice to handbook
- [!15009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15009) Adjustments to alliances handbook
- [!15005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15005) Update ops-backend/configure/index to match new template
- [!15004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15004) fixed tiny style typo on EJ README
- [!15002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15002) ak-move-press-boiler-plate-section
- [!14997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14997) fix spelling mistakes
- [!14996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14996) Update Plan backend handbook page
- [!14993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14993) Fixing empty alliances handbook page and adding inbound request section
- [!14991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14991) Updated prioritized board link for Manage
- [!14984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14984) fix dalia's role and update monitor team name
- [!14982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14982) update blog handbook
- [!14946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14946) Add quality team’s project management process
- [!14753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14753) Capabilities are a grab bag.
- [!14652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14652) updated wording for term departures

### 2018-10-01
- [!14971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14971) Clarify instructions for adding features
- [!14962](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14962) Fix a few typos in source/handbook/engineering/quality/test-engineering/index.html.md
- [!14960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14960) Add team members to Monitor team page
- [!14956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14956) Update handbook for Content Hack Day Q3
- [!14953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14953) Build individual team pages using data from team.yml
- [!14947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14947) Tiny grammar fix turned reword for meetup schedule
- [!14943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14943) Add link to dir readme and update CI/CD names
- [!14940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14940) Template for team pages
- [!14938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14938) RACI in README
- [!14933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14933) Adding the MSA or EULA effective date if the client/prospect will not sign our order form.
- [!14907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14907) updated Terminus section and swag.
- [!14789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14789) Update blog handbook
- [!14303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14303) Fix link + better description for adding blog posts social sharing image

### 2018-09-30
- [!14937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14937) Management span.
- [!14936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14936) Emphasizing velocity
- [!14926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14926) 20180929 mpm section updates

### 2018-09-29
- [!14912](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14912) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14911) fix URLs without titles
- [!14707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14707) Clarify that GitLab Gold is available for private accounts
- [!14617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14617) Added test engineering, plans and process to the Quality handbook

### 2018-09-28
- [!14871](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14871) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14867](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14867) Update swag requests
- [!14862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14862) Remove arrows from Forrester graphics
- [!14858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14858) Update source/handbook/tools-and-tips/index.html.md
- [!14857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14857) Update source/handbook/marketing/product-marketing/competitive/application-security/index.html.md
- [!14852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14852) Add CFO to approval process
- [!14844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14844) Resolve "Correcting a grammar mistake in the Gitlab handbook."
- [!14813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14813) Adding an alliances handbook
- [!14808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14808) Updates to engagement rules and adding MQLs
- [!14580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14580) clarifying spending money
- [!14265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14265) adds gdpr article 15 workflow
- [!14027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14027) Create "How to Respond to Tickets" Page

### 2018-09-27
- [!14859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14859) Added detail around "value" to the person.
- [!14851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14851) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14850](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14850) Updated Email Communication Policy section
- [!14837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14837) Clarifying that if no level is indicated, it is intermediate
- [!14833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14833) Update Forrester list to remove CI tools. It's above.
- [!14830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14830) Update remove ARO from future reports
- [!14824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14824) Update source/handbook/marketing/marketing-sales-development/sdr/index.html.md
- [!14822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14822) Wiring Design to Infra main page.
- [!14817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14817) Add notes about ambitious planning to product handbook and direction page
- [!14816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14816) Fix "Geo development" link
- [!14807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14807) Update to include lessons learned in handbook
- [!14805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14805) update to pro rated language in director comp
- [!14796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14796) infra/5088: storage nodes design
- [!14784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14784) initial infra design and template
- [!14296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14296) Fix broken link to upgrade metrics on distribution page

### 2018-09-26
- [!14788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14788) Move content marketing to corporate marketing
- [!14777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14777) Reword Create team description and link to 2019 product vision.
- [!14775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14775) Adds two extra points to the how to become a maintainer guide
- [!14774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14774) Remove step to unlabel from Distribution triage documentation
- [!14773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14773) Update post event f/up to match process
- [!14770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14770) Links updates and change to APAC call time
- [!14768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14768) Dalia's readme
- [!14733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14733) example to clarify stock options for promotion
- [!14608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14608) Add 'team calendar' section to monitoring team page

### 2018-09-25
- [!14757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14757) Fix CI/CD typos
- [!14754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14754) Adjusting the language for transfers to add clarity
- [!14743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14743) Update source/handbook/git-page-update/index.html.md
- [!14740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14740) GDPR Workflow Fixes
- [!14730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14730) Update handbook with training docs from week 1
- [!14723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14723) Update ops backend configure team page
- [!14714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14714) Fix links which describe how we decide what is paid and what tier
- [!14713](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14713) Add GCP Security Guidelines Policy link to internal doc in handbook
- [!14712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14712) Add security-aware dev report to archive
- [!14673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14673) Rename Community Marketing to Community Relations in documentation
- [!14667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14667) Fix minor typo on Communication page

### 2018-09-24
- [!14704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14704) Add dev research report to archive
- [!14702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14702) Example for candidate on how to edit the handbook
- [!14694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14694) Move Secret Snowflake page to the People Operations section
- [!14692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14692) additional wording to internal transfers.
- [!14688](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14688) Update editing paragraph to get rid of mailto
- [!14687](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14687) Update training paragraph.
- [!14686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14686) Clarify role of backups
- [!14681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14681) add results from survey page
- [!14676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14676) Metric Meeting Format
- [!14675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14675) Update segmentation in Business Ops Section
- [!14672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14672) Adds GitLab instance survey report under "GitLab.com"
- [!14642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14642) Add Bora as the OSS Project expert
- [!14614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14614) Adding HRBP to review discretionary bonus
- [!14558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14558) Update index.html.md
- [!14402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14402) Update EDU process

### 2018-09-22
- [!14660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14660) add demo link
- [!14656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14656) Minimize pressure to work long hours
- [!13955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13955) GitLab 11.3 Release Post

### 2018-09-23
- [!14653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14653) Update source/handbook/finance/operating-metrics/index.html.md

### 2018-09-21
- [!14649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14649) Added logo specifications to Design System
- [!14644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14644) Update the email request process
- [!14633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14633) Add EMEA Support / Sales / CS cross functional meeting
- [!14627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14627) Fix broken link for Jetbrains license management in "Spending Company Money" section
- [!14625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14625) documenting secret managemnent
- [!14621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14621) Add Security Products header and anchor
- [!14618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14618) Infrastructure Blueprint
- [!14607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14607) Rollover to Betterment

### 2018-09-20
- [!14601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14601) non-reimbursement expenses
- [!14600](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14600) Update index.html.md.
- [!14595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14595) Add support and docs cross functional meeting
- [!14590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14590) Rename engineering_areas to capabilities
- [!14586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14586) Clarify experience factor template team member input section
- [!14565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14565) adds cross functional meetings
- [!14529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14529) Update:  Adding Abuse team to workflow
- [!14433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14433) Move Monitor team sections around, remove technology
- [!14403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14403) Changes to the Geo Team frontpage

### 2018-09-19
- [!14564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14564) contractor invoice
- [!14562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14562) add new slack channel
- [!14560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14560) Add deprecaed doc to the sales enablement training page on PMM part of website
- [!14551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14551) Add community contributions to product priority
- [!14546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14546) Update fixing errors by deleting section
- [!14533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14533) Fix playlist link
- [!14527](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14527) Update Marketing Handbook links
- [!14523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14523) Fix several issues with communication page.
- [!14517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14517) Removed link to article that doesn't exist anymore
- [!14502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14502) Flow one
- [!14498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14498) handbook: support: add chair, internal requests
- [!14479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14479) Define the difference between "team leads" and "managers" at GitLab
- [!14449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14449) Added Vendor Contract process

### 2018-09-18
- [!14508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14508) Update analyst handbook page with training
- [!14507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14507) add links to pipe-to-spend links to Update source/handbook/business-ops/index.html.md
- [!14500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14500) entire devops lifecycle
- [!14495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14495) Warn about Slack private groups
- [!14486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14486) fix broken greenhouse link
- [!14484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14484) remove referral, update greenhouse with zoom, removed sections in lever already in gh
- [!14482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14482) Remove Robert from the Quality team page
- [!14478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14478) Update update with education section of analyst reports
- [!14476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14476) add domestic partner hsa information
- [!14469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14469) Removing AZ
- [!14467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14467) Define EULA Acronym
- [!14466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14466) Add TOC.
- [!14465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14465) Adding Outreach.io Call disposition info to handbook
- [!14453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14453) Entire DevOps Lifecycle and 200 percent messaging
- [!14452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14452) Secure Team - Add YouTube playlist
- [!14450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14450) Jj removing maturitymodel
- [!14448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14448) AR Workflow for refunds
- [!14442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14442) Move redirect setup policy to the handbook-usage page
- [!14333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14333) Fix link to image, and make Markdown source conform more to standards.
- [!14312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14312) Explain why we still use "Security Products"
- [!14204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14204) Fixing a typo in the handbook
- [!14190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14190) Update the number of contributors to GitLab CE
- [!14128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14128) Add bullet point in Communication linking to Chat channel page
- [!14037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14037) Update "hiring SOs/family members" policy to address concerns of privacy

### 2018-09-17
- [!14446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14446) Add social media profile assets
- [!14441](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14441) Replacing growth marketing with pipe-to-spend
- [!14440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14440) Fix broken links to the /hiring/roles/ folder
- [!14439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14439) Fix broken link to success_image
- [!14438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14438) added sales enablement
- [!14435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14435) Add new experience factor worksheet with soft skills, team member inputs
- [!14423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14423) more updates to reseller marketing kit
- [!14420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14420) KL Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14412) add inmail issue template Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!14411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14411) Cigna ID Cards
- [!14409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14409) Revert "Merge branch 'complete-devops-lifecycle' into 'master'"
- [!14404](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14404) PS Opps must have a Parent Opp
- [!14400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14400) Reviewers and Maintainers process
- [!14394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14394) Add instructions for tool logos on home and compare pages
- [!14391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14391) swag updates
- [!14387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14387) move capabilities from pricing page
- [!14382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14382) Update handbook pages via new community contribution workflow
- [!14354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14354) Updating Barbie's schedule Hours preferences under EA
- [!14344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14344) Move Secure page to direction
- [!14340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14340) Add theme for Ocotober frontend call
- [!14316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14316) Flatten categories and capabilities to make it easier to communicate and reason about.
- [!14197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14197) Add advocate-for-a-day Slack channel to CM handbook

### 2018-09-16
- [!14390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14390) moving some swag stuff to just be in corporate marketing
- [!14388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14388) Update source/handbook/marketing/product-marketing/reseller-kit/index.html.md
- [!14384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14384) Resolve "Comparison PDF Generation is Broken"
- [!14334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14334) Update mentions of Complete Devops

### 2018-09-15
- [!14380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14380) Add notice about adding links to categories and capabilities
- [!14377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14377) Add DevOps to "Is it similar to GitHub?" message
- [!14352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14352) Added note about 'partially' flag to 'tools' field.

### 2018-09-14
- [!14367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14367) 401k Information
- [!14361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14361) Standardize on complete DevOps lifecycle
- [!14331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14331) Correct "setup" and "set up" grammar sitewide
- [!14330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14330) Link into Greenhouse information, remove more references to Lever
- [!14323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14323) Add pipe-to-spend to Marketing Metrics section in source/handbook/business-ops/index.html.md
- [!14317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14317) Update content and links to reseller marketing kit
- [!14214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14214) Make it simpler
- [!14207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14207) Replacing availability with Team Meetings Calendar.
- [!14161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14161) Update meltano team roles

### 2018-09-13
- [!14301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14301) Update Exec preferences
- [!14298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14298) Minor Manage Team page update
- [!14290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14290) added manual way process for Non ACH and Non US Residents
- [!14287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14287) fix image
- [!14283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14283) moved swag from fm
- [!14282](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14282) moved swag to corp marketing
- [!14279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14279) Revert note about registry not working with canary env
- [!14273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14273) Adding notes that Parent account is the Ultimate Parent Account in SFDC
- [!14266](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14266) Add devops interview report to archive
- [!14262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14262) Update MVC section with thin content details
- [!14242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14242) Resolve "Update categories to use stub vs name"
- [!14212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14212) Support workflow: needs-org: add resync
- [!14209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14209) remove link written over
- [!14152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14152) Update Supporting Community Individuals CA handbook
- [!14138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14138) support workflow: update/rename 2FA
- [!14023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14023) Creating reseller marketing kit page
- [!13996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13996) handbook: add link to support email workflow

### 2018-09-12
- [!14261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14261) Changing tone
- [!14258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14258) Add link to Tommy's README
- [!14250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14250) Remove hiring order list
- [!14244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14244) comparison summary instructions
- [!14241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14241) editing XDR coaching
- [!14236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14236) Fix services .com support bootcamp link
- [!14234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14234) fix headers on greenhouse page
- [!14229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14229) adding XDR coaching to the enablement page
- [!14223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14223) Update source/handbook/engineering/career-development/index.html.md
- [!14216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14216) Add partial reimbursement language
- [!14203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14203) Update field marketing page
- [!14179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14179) Specifying confidentiality rule for security issues
- [!14137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14137) adds information request workflow
- [!14127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14127) Add new structure for Monitoring Team handbook page

### 2018-09-11
- [!14218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14218) featured blog post instructions
- [!14211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14211) Eric's Links
- [!14208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14208) Servant leader
- [!14194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14194) update referral info for GH
- [!14193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14193) Updates to Benefits with Lumity
- [!14184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14184) Never too high, or too low
- [!14181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14181) clarify website scope and ownership
- [!14180](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14180) Clarifying non-traditional background
- [!14151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14151) Update source/handbook/engineering/infrastructure/production/index.html.md
- [!14079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14079) Added some additional language to the initiative part.  Managers and employees…

### 2018-09-10
- [!14175](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14175) clarify prime commuting times
- [!14168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14168) Greenhouse-commits
- [!14146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14146) Added retro info to Manage page
- [!14145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14145) Update zoom info
- [!14143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14143) Added Functional Approval defintion
- [!14130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14130) Add sre report to ux archive
- [!14090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14090) Update security paradigm link
- [!14062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14062) Update source/handbook/engineering/career-development/index.html.md

### 2018-09-07
- [!14135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14135) Add Lumity Payroll Processes
- [!14118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14118) Documenting Iconography as part of our Marketing Design System
- [!14105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14105) Added interviews with churned users report
- [!14098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14098) Fix a typo on the Quality page
- [!14086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14086) adding some language around career development roles and responsibilities for…
- [!14008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14008) Offboarding Procedures: updating for clarity

### 2018-09-06
- [!14100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14100) Update index.html.md
- [!14082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14082) Updates to monitoring page
- [!14080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14080) Resolve "move case study publishing process to customer reference handbook"
- [!14078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14078) Rename Product Core Values to Product Principles so it's not confused with company values
- [!14073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14073) update the campaign member status
- [!14071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14071) Fixes various links for the public dashboards
- [!14070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14070) Fix broken arch image and atom is opionated about whitespace/punctuation.
- [!14069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14069) Add note on manually scheduling coffee breaks. Improve instructions to add new team members via the Web IDE.
- [!14057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14057) Note how to request an account on Staging. There has been multiple Slack…
- [!14055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14055) Rename Security Features page to Security Paradigm
- [!14046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14046) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14044) Add social takeover notes
- [!14042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14042) Update process to be more accurate
- [!13975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13975) Add security features page

### 2018-09-05
- [!14043](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14043) Fix small typo in source/handbook/engineering/career-development/index.html.md
- [!14040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14040) crosslink campaign expense policy to finance handbook
- [!14039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14039) deprecate spreadsheet
- [!14031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14031) Update source/handbook/values/index.html.md
- [!14022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14022) reupdate company call
- [!14018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14018) Add gitlab-ce#48847 to UX Design archive
- [!14012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14012) Add section on career coaching for engineering, senior dev template
- [!14006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14006) Career Dev components
- [!14004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14004) Eric readme
- [!14003](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14003) Add github ci cd faq
- [!14000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14000) corrected run-on sentence on spending company money
- [!13999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13999) Adds research report to research archive
- [!13998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13998) Add new depts and allocation methodology
- [!13990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13990) Typo
- [!13970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13970) Fix links to dashboard graphs

### 2018-09-04
- [!13972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13972) Rename eShares to Carta
- [!13971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13971) Pension Edits

### 2018-09-03
- [!13960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13960) Clarify Sales OPS link to BizOPS

### 1970-01-01
- [!13958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13958) Fix typo in confidential information section
