---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Product Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
   - [GitLab Analyst Newsletter October 2018](https://docs.google.com/document/d/1MrAsx2UgIQjwbIEplrqvUvZajEzVRuKfZOGhU-qTExw/edit?usp=sharing)
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## Accessing analyst reports

Most analyst companies charge for access to their reports. 

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](https://about.gitlab.com/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLabber and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.


## What the analysts are saying about GitLab

This section contains highlights from areas where the analysts have rated GitLab in comparison to other vendors in a particular space. The highlights and lessons learned are listed here.  For more information click the link to go to the page dedicated to that report.

### [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/)

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

[The complete list of documents by topic can be found here.](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/sales-training/)


## Areas for expanded coverage

As GitLab grows and our product capabilities expand, we are engaging with more analysts in more companies, on a wider array of topics. The following is a list of topics and analyst companies appearing on our radar. The information here shows the date the report was published as well as the criteria and vendors considered for the report.


### Forrester:

##### Wave: Configuration Management Software for Infrastructure Automation, October 11, 2017
- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)
- Criteria for inclusion: Evaluated solutions had to provide a minimum set of capabilities, including deployment, configuration modeling, monitoring
and governance, and community support.
- Element configuration management tools will converge with CDRA - Forrester expectation

##### Wave: Modern Application Functional Test Automation Tools, December 5, 2016
- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)
- Criteria for inclusion: Cross-browser FTA and mobile testing capabilities, UI and API FTA capabilities

##### Wave: Static Application Security Testing, December 12, 2017
- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)
- Criteria for inclusion: a comprehensive, enterprise-class SAST tool, interest from Forrester clients
- Language support and depth of results are key differentiators.
- Companies traditionally used SAST tools late in the software development lifecycle (SDLC) to scan products for vulnerabilities in proprietary code. Now developers need early remediation advice throughout the SDLC.

##### Wave: Strategic Portfolio Management (SPM) Tools, September 20, 2017
- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)
- Criteria for inclusion: Integration of strategic planning capabilities, including goals, assessment, decision support, validation, gap analysis, and collaboration; portfolio management capabilities such as alignment, integrated operational alignment, portfolio analytics and data security; usability capabilities.
- Planning disciplines are transitioning from annual to continuous; PPM is too narrow of a focus.
- SPM tools support planning at a unified level.
- SPM underpins organizations' embrace of Agile planning.

##### Wave: Enterprise Collaborative Work Management, October 17, 2016
- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)
- Criteria for inclusion: Combine collaboration and task management to organize planned and unplanned work; Ability to create work plans out of ad hoc conversations or individual activities; how are tools used to work with external users as well as internal.


### Gartner: 

##### Software Change and Configuration Management Software
- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [Link to report](https://www.gartner.com/document/3118917)
- Market Guide update initiated March 2018.

##### Continuous Configuration Automation Tools
- Market Guide: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)

##### Software Test Automation
- Leaders: MicroFocus, Tricentis
- [Link to 2016-11-15 report](https://www.gartner.com/document/3512920)
- [Link to 2017-11-20 report](https://www.gartner.com/document/3830082)

##### Performance Testing
- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [Link to report](https://www.gartner.com/document/3133717)

##### Application Performance Monitoring Suites
- Leaders: New Relic
- [Link to report](https://www.gartner.com/doc/3551918)

##### Application Security Testing
- Leaders: Micro Focus, CA Technologies (Veracode), Checkmarx, Synopsys, IBM
- [Link to 2017-02-28 report](https://www.gartner.com/doc/3623017)
- [link to 2018-03-19 report](https://www.gartner.com/doc/3868966/magic-quadrant-application-security-testing)

##### Project Portfolio Management
- Leaders: Planview, CA Technologies, Changepoint
- [Link to report](https://www.gartner.com/document/3728917)

##### Enterprise Agile Planning Tools
- Leaders: CA, Atlassian, VersionOne
- [Link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

##### Container Management Software
- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [Link to report](https://www.gartner.com/document/3782167)

##### Enterprise Application Platform as a Service
- Leaders: Salesforce, Microsoft
- [Link to report](https://www.gartner.com/document/3263917)

##### Data Science Platforms
- Leaders: IBM, SAS, RapidMiner, KNIME
- [Link to report](https://www.gartner.com/doc/3606026)