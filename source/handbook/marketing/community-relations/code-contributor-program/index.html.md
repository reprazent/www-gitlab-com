---
layout: markdown_page
title: "Code Contributor Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}



### Mentor program
After the first merged MR, make an offer (in an outreach email per the template below) to pair the new contributor with an experienced mentor from the community.  This is for a limited time period only (2 weeks) so that mentors are not overly burdened.  

Currently, the mentors consist of Core Team with plans to expand the mentor group.  

### Working with the Core Team
There will be a monthly call with members of the [Core Team](https://about.gitlab.com/core-team/) using the Zoom conferencing tool and meetings will also be recorded. Since Core Team members are spread out in different time zones, meeting times will be rotated.   

[Service Desk](https://gitlab.com/gitlab-core-team/general/issues/service_desk) will be used as a communication tool for Core Team members. Anyone can open issues in the Service Desk or send an email to `incoming+gitlab-core-team/general@incoming.gitlab.com`  Core Team members who signed NDAs will also have access to GitLab Slack channels.  

### Contributor blog post series

Goal is to publish a regular blog post featuring contributors from the community.  The format will be a [casual Q&A with a community member](https://about.gitlab.com/2018/08/08/contributor-post-vitaliy/) and will be posted on the [GitLab blog page](https://about.gitlab.com/blog/). 

When developing a blog post, follow the [blog guidelines](/handbook/marketing/blog/#blog-style-guidelines). 

### Contributor channel

A GitLab community room is available on [Gitter](https://gitter.im/gitlabhq/community) for people interested in contributing to GitLab.  This is open to everyone to join.

### Hackathons

We will be organizing a quarterly Hackathon for GitLab community members to come together to work on merge requests, participate in tutorial sessions, and support each other on the [GitLab community channel](https://gitter.im/gitlabhq/community).  Agenda, logistics, materials, recordings, and other information for Hackathons will be available on the [Hackathon project pages](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon). 

### Outreach Email Templates

#### Outreach after the first merged MR 

```
Hello NAME,

I’m reaching out to both thank and congratulate you on your first Merge Request (MR) to help improve GitLab.  We appreciate everyone's effort to contribute to the GitLab, especially when it's an individual initiative and we are blessed to have such a wonderful community.  I wanted to offer you a couple of things as you’re getting started with your code contributions to GitLab.

1. Let us know if you'd like to receive the latest GitLab mug with a special hashtag to celebrate your first merged MR.  Please go to [URL] to submit your order (the quantity should be 1), and we will take care of the rest.  When you receive the merchandise, it would be great if you can make a post on twitter with your photo of the mug plus '@gitlab' & '#myfirstMRmerged' in the tweet. 
2. Please let me know if you’d like to be paired with one of the experienced GitLab community members (a mentor) for a few weeks to help with your future contributions to GitLab.  

Thanks again for your first MR to GitLab and welcome to the GitLab community!

Sincerely,
YOUR_NAME
```

#### Outreach to inactive contributors

```
Hello NAME,

I’m reaching out to you as we have not seen a merged MR from you since DATE.  The GitLab community would not be the same if it weren’t for contributors like you.  If you haven’t done so lately, you can look for issues that are “Accepting Merge Requests” as we would welcome your help on these.  

Please let me know if you have any questions and I look forward to your continued involvement in the GitLab community.  

Sincerely,
YOUR_NAME
```

