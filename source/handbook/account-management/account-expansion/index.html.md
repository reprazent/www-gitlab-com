---
layout: markdown_page
title: "Account Mapping and Expansion Plan Template"
---
The contents of this page have been moved to the [Customer Success Handbook](/handbook/customer-success/account-management/account-expansion)