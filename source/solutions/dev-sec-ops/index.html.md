---
layout: markdown_page
title: "Integrating Security into DevSecOps"
---

## Application Security is hard - when it is a separate process

### You want to test everything but you...

* Can’t afford to test all of your code or have everyone use it
* Lack security analysts to validate results, prioritize and triage
* Experience friction between app sec and engineering that hinders remediation
* Already have found more vulnerabilities than you have time to fix.

### So you may...

* Only test externally-facing apps because they have higher risk
* Only test mission critical apps
* Only test major releases
* Focus on one testing method alone

### Even though you know that...

* Hackers can traverse laterally across your enterprise. They look for the weakest link so internally-facing apps must be tested too.
* Different types of scans are best at finding different types of vulnerabilities.
* Exploits change all the time. Penetration testing only tests a snapshot in time.
* You must strike a balance between risk and business agility.

## Benefits of an integrated approach

Balancing business velocity with security is possible. With GitLab, application security testing is built into the CI/CD process. Every merge request is scanned for vulnerabilities in your code and that of its dependencies. This enables some magic to happen:

* Every piece of code is tested upon commit, without incremental cost.
* One source of truth for both the developer and the security pro promotes collaboration and empowers the developer to identify vulnerabilities within their own workflow.
* The developer can remediate now, while they are still working in that code, or create an issue with one click.
* The dashboard for the security pro is a roll-up of vulnerabilities remaining that the developer did not resolve on their own.
* Vulnerabilities can be efficiently captured as a by-product of software development.
* A single tool also reduces cost over the approach to buy, integrate and maintain point solutions.
 
Learn more about the [GitLab Secure Paradigm](https://about.gitlab.com/direction/secure/#security-paradigm).

## Continuous security testing within CI/CD

When using GitLab CI/CD, every merge request is automatically testing using the following methods.

### Static Application Security Testing (SAST)

<table style="width:100%; border:0px">
   <tr>
      <td style="width:50%">
      <ul>
      <li>Scan the application source code and binaries to spot potential vulnerabilities.</li>
      <li>Because these open source tools are installed as part of GitLab Ultimate, there are no added costs.</li>
      <li>Vulnerabilities are shown in-line with every merge request and results are collected and presented as a single report.</li>
      <li>Evaluate vulnerabilities from the GitLab pipeline and dismiss or create an issue with one click.</li>
      </ul></td>
      <td rowspan="4"><img src="/images/secure/sast-screen-shot.png"></td>
    </tr>
</table>

### Dynamic Application Security Testing (DAST)

<table style="width:100%; border:0px">
    <tr>
      <td style="width:50%">
      <ul>
      <li>Dynamic scanning earlier in the SDLC than ever possible, by leveraging the review app CI/CD capability of GitLab.</li>
      <li>Test running web applications for known runtime vulnerabilities.</li>
      <li>Users can provide HTTP credentials to test private areas.</li>
      <li>Vulnerabilities are shown in-line with every merge request.</li>
      </ul></td>
      <td rowspan="4"><img src="/images/secure/dast-findings-detail.png"></td>
    </tr>
</table>

### Open Source Dependency Scanning

<table style="width:100%; border:0px">
    <tr>
      <td style="width:50%">
      <ul>
      <li>Analyze external dependencies (e.g. libraries like Ruby gems) for known vulnerabilities on each code commit with GitLab CI/CD.</li>
      <li>Identify vulnerable dependencies needing updating.</li>
       <li>Vulnerabilities are shown in-line with every merge request.</li>
      </ul></td>
      <td rowspan="4"><img src="/images/secure/dependency-findings-detail-full-screen.png"></td>
    </tr>
</table>

### Container Scanning

<table style="width:100%; border:0px">
    <tr>
      <td style="width:50%">
      <ul>
      <li>Check Docker images for known vulnerabilities in the application environment.</li>
      <li>Avoid redistribution of vulnerabilities via container images.</li>
      <li>Vulnerabilities are shown in-line with every merge request.</li>
      </ul></td>
      <td rowspan="4"><img src="/images/secure/container-findings-detail.png"></td>
    </tr>
</table>

Why stop there? While GitLab runs application security scans on every code commit, why not also verify compliance with open source licenses.  

### License Management

<table style="width:100%; border:0px">
    <tr>
      <td style="width:50%">
      <ul>
      <li>Automatically search project dependencies for approved and blacklisted licenses defined by your policies.</li>
      <li>Custom license policies per project.</li>
      <li>License analysis results are shown in-line for every merge request for immediate resolution.</li>
      </ul></td>
      <td rowspan="4"><img src="/images/secure/license-management-in-mr.png"></td>
    </tr>
</table>

Want to know where we're headed? [Learn about the future of GitLab's Secure capabilities](https://about.gitlab.com/direction/secure/)

### For more details, check out the product documentation

* [Static Application Security Testing](https://docs.gitlab.com/ee//user/project/merge_requests/sast.html)
* [Dynamic Application Security Testing](https://about.gitlab.com/product/dynamic-application-security-testing)  
* [Dependency Scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html)
* [Container Scanning](https://about.gitlab.com/product/container-scanning)
* [License Management](https://about.gitlab.com/product/license-management)

### Resources

* [Blog article: See how integration is the key to successful DevSecOps](https://about.gitlab.com/2018/09/11/what-south-africa-taught-me-about-cybersecurity/)
* [Comparisons](about.gitlab.com/comparisons)
* [11.1 Release Radar with Security Level-set](https://about.gitlab.com/webcast/monthly-release/gitlab-11.1---security)
* [Security Presentation](https://about.gitlab.com/handbook/marketing/product-marketing/#security-deck)
